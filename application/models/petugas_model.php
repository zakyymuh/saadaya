<?php class Petugas_model extends CI_Model {
 function get_all_info($id){
   $this->db->select('*');
   $this->db->from('petugas');
   $this->db->where('id_petugas',$id);
   $query = $this->db->get();
   if($query->num_rows()===1){
	return $query->result();
   }
   else{
	return false;
   }
}
 function tambah_grup($data){
	$query = $this->db->insert("grup",$data);
	if($query){
		return true;
	}
	else{
		return false;
	}
 }
 function check_grup($var){
	$this->db->select("*");
	$this->db->from("grup");
	$this->db->where("id_grup",$var);
	$this->db->where("id_grup NOT LIKE '%T%'");
	$return = $this->db->get();
	if($return){
		return true;
	}
		return false;
 }
 function get_grup_utama(){
	$this->db->select("*");
	$this->db->from("grup");
	$this->db->where("id_grup NOT LIKE '%T%'");
	$query = $this->db->get();
	return $query->result();
 }
 function get_grup_tambahan(){
	$this->db->select("*");
	$this->db->from("grup");
	$this->db->like("id_grup","T","both");
	$this->db->order_by("nama", "desc"); 
	$query = $this->db->get();
	return $query->result();
 }
 }
?>