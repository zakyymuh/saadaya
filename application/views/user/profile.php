<style>
	.sosial-button {
		margin-top:10px;
	}
</style>
	<?php
	foreach($sosmed as $row){
	$fb = $row->facebook;
	$tw = $row->twitter;
	$ig = $row->instagram;
	$gp = $row->googleplus;
	$li = $row->linkedin;
	$gh = $row->github;
	}									
  foreach($profile as $row){
   $nama = $row->nama;
   $email = $row->email;
   $bio = $row->bio;
   $alamat = $row->alamat;
   $hp = $row->hp;
   $job = $row->job;
   $ed = $row->education;
   $tgl = $row->tanggal_lahir;
   $profile_picture = $row->profile_picture;
  }
	foreach($data as $row)
	$nama2 = $row->nama;
	if($nama == $nama2){
 ?>
<div class="content-wrapper" style="min-height:1098px;">     
   <section class="content-header">
          <h1>
            Profile
            <small>Update your information</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?= base_url()?>user/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Profile</li>
          </ol>
        </section>
 <?php
 }
 ?>
 <section class="content">
					
	<div class="row">
		<div class="col-md-4">
		<div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active">
                  <h3 class="widget-user-username"><?= $nama?></h3>
                  <h5 class="widget-user-desc"><?= $hp?></h5>
                </div>
                <div class="widget-user-image">
                  <img class="img-circle" src="<?= base_url()?>assets/image/upload/<?=$profile_picture?>" alt="User Avatar">
                </div>
                <div class="box-footer">
                  <div class="row">
                    <div class="col-sm-4 col-xs-4 border-right">
                      <div class="description-block">
                        <h5 class="description-header">122</h5>
                        <span class="description-text">JOIN</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-4 col-xs-4 border-right">
                      <div class="description-block">
                        <h5 class="description-header">3</h5>
                        <span class="description-text">EVENTS</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-4 col-xs-4">
                      <div class="description-block">
                        <h5 class="description-header">#222</h5>
                        <span class="description-text">RANKS</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                  </div><!-- /.row -->
				  <div class="text-center sosial-button">
                    <a class="btn btn-social-icon btn-facebook" href="https://www.facebook.com/<?= $fb ;?>" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a class="btn btn-social-icon btn-twitter" href="https://www.twitter.com/<?= $tw ;?>" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a class="btn btn-social-icon btn-instagram" href="https://www.instagram.com/<?=$ig ; ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a class="btn btn-social-icon btn-google"  href="https://www.google.com/<?=$gp ;?>" target="_blank"><i class="fa fa-google-plus"></i></a>
                    <a class="btn btn-social-icon btn-github"  href="https://www.github.com/<?=$gh ;?>" target="_blank"><i class="fa fa-github"></i></a>
                    <a class="btn btn-social-icon btn-linkedin"  href="https://www.linkedin.com/<?=$li ;?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
			  <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">About Me</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <strong><i class="fa  fa-black-tie margin-r-5"></i>  Job</strong>
                  <p class="text-muted"><?= $job;?>
                  </p>

                  <hr>
				  <strong><i class="fa fa-book margin-r-5"></i>  Education</strong>
                  <p class="text-muted">
                    <?= $ed;?>
                  </p>

                  <hr>

                  <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                  <p class="text-muted"><?= $alamat;?></p>

                  <hr>
                  <strong><i class="fa fa-file-text-o margin-r-5"></i> Bio</strong>
                  <p><?= $bio;?></p>
				  
                </div><!-- /.box-body -->
              </div>
		</div>
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-12">
				<?php
					if($this->session->flashdata('item')) {
					$message = $this->session->flashdata('item');
					?>
					   <div class='alert <?= $message['class'];?>'><?= $message['message'];?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
					<?php } ?>
				</div>
			</div>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#settings" data-toggle="tab">Profile</a></li>
                  <li><a href="#password" data-toggle="tab">Password</a></li>
                  <li><a href="#profilepic" data-toggle="tab">Profile Picture</a></li>
                  <li><a href="#sosmed" data-toggle="tab">Sosial Media</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="settings">
				  <!-- belum ada ganti profile -->
					<?php $att = array("class"=>'form-horizontal');
						  echo form_open("user/#",$att); ?>
				   <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputName" placeholder="<?= $nama ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputEmail" placeholder="<?= $email ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPhone" class="col-sm-2 control-label">No Hp</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName" placeholder="<?= $hp ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputBio" class="col-sm-2 control-label">Alamat</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="inputExperience" placeholder="<?= $alamat ?>"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputSkills" class="col-sm-2 control-label">Tgl Lahir</label>
                        <div class="col-sm-10">
                          <input type="date" class="form-control" id="inputSkills" placeholder="<?= $tgl ?>" value='<?= $tgl?>'>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputBio" class="col-sm-2 control-label">Bio</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="inputExperience" placeholder="<?= $bio ?>"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" name="submit" class="btn btn-danger btn-flat">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div><!-- /.tab-pane -->
				  <div class="tab-pane" id="password"> 
				  <?= form_open("user/act_password",$att);?>
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Lama</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="password" name="oldpassword" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label">Baru</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="passwordbaru" name="newpassword" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Ulang Baru</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="passwordbarut" name="repassword" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <input type='submit' name='submit' value='Change' class="btn btn-danger btn-flat">
                        </div>
                      </div>
                    </form>
					<script>
						var password = document.getElementById("passwordbaru"),confirm_password = document.getElementById("passwordbarut");
						function validatePassword(){
							if(password.value.length < 6){
							password.setCustomValidity("Password tidak boleh kurang dari 6 digit !");
							}
							else {
								password.setCustomValidity('');
							}
							
							if(password.value != confirm_password.value) {
							confirm_password.setCustomValidity("Password yang anda masukan tidak sama !");
							} 
							else {
								confirm_password.setCustomValidity('');
							}
						}
						password.onchange = validatePassword;
						confirm_password.onkeyup = validatePassword;
						password.onkeyup = validatePassword;
</script>
				  </div><!-- tab pane end -->
				  <div class="tab-pane" id="profilepic"> 
				  <?= form_open_multipart("user/upic_profile",$att);?>
				  <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Upload</label>
                        <div class="col-sm-10">
                          <input type="file" class="form-control"  name='filefoto'>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <input type='submit' name='submit' value='Upload' class="btn btn-flat btn-danger">
                        </div>
                      </div>
				  </form>
				  </div><!-- tab pane end -->
				
				  <div class="tab-pane" id="sosmed">
				  <?= form_open("user/usosialmedia",$att) ?>
				 	<div class="col-sm-12 col-md-12">
						<p class="text-muted">Isilah dengan menggunakan nickname/id anda seperti 
						https://facebook.com/<strong>zakyymuh</strong></p>
						</div>
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Facebook</label>
                        <label for="inputName" class="col-sm-4 control-label">https://www.facebook.com/</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="facebook" name="facebook" value="<?= $fb;?>" >
                        </div>
                      </div>
					  <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Twitter</label>
                        <label for="inputName" class="col-sm-4 control-label">https://www.twitter.com/</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="twitter" name="twitter" value="<?= $tw;?>" >
                        </div>
                      </div>
					  <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Instagram</label>
                        <label for="inputName" class="col-sm-4 control-label">https://www.instagram.com/</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="instagram" name="instagram" value="<?= $ig;?>" >
                        </div>
                      </div>
					  <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Google+</label>
                        <label for="inputName" class="col-sm-4 control-label">https://www.facebook.com/</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="google" name="google" value="<?= $gp;?>" >
                        </div>
                      </div>
					  <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Github</label>
                        <label for="inputName" class="col-sm-4 control-label">https://www.github.com/</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="github" name="github" value="<?= $gh;?>" >
                        </div>
                      </div>
					  <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">LinkedIn</label>
                        <label for="inputName" class="col-sm-4 control-label">https://www.LinkedIn.com/</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="linked" name="linkedin" value="<?= $li;?>" >
                        </div>
                      </div>
                      <div class="form-group">
						
                        <div class="col-sm-offset-6 col-sm-4">
                          <input type='submit' name='submit' value='Change' class="btn btn-danger btn-flat">
                        </div>
                      </div>
                    </form>
				  </div>
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div>
	</div>
    </div>
	</section><!-- /.content -->
      </div><!-- /.content-wrapper -->