<script type="text/javascript">
<?php  echo "var onHome = true"; ?>
</script>

<div class="banner" id="top" style="background: url(assets/images/banner.jpg)">
<span class="text">SAUNG BUDAYA<br>UNIVERSITAS KOMPUTER INDONESIA</span>
<span class="text-mini">BERSAMA SADAYA, KITA BANGKITKAN KESENIAN TRADISIONAL</span>
<?php
  
            $date1=date_create(date('Y-m-d'));
            $date2=date_create("2017-10-09");
            $diff=date_diff($date1,$date2);
            $diff2 = $diff->format("%R%a");
            if($diff2 < 0){
            }
            else{
              if($diff2 == 0){
              echo '<div class="button"><a href="daftar.php">PENDAFTARAN ANGGOTA BARU - HARI TERAKHIR </a></div>';
              }
              else{
              echo '<div class="button"><a href="daftar.php">PENDAFTARAN ANGGOTA BARU - '.$diff->format("%a HARI LAGI").'</a></div>';
              }
              
            }
?>
</div>
<div class="section">
  <div class="container">
    <div class="section-title">ANGGOTA SADAYA<Strong> ANGKATAN 11.</Strong></div>
    <div class="divider"></div>

    <div class="content" >
      <div class="row">
        <div class="text">
          <p>Abeyola Palupi |
Abnisya Wahyuningtyas |
Afina Nuraini Nadhirah  |
Afiva Harsini Zahra |
Agung Irawan  |
Agung Setiawan  |
Akhmad Irvan Attorik  |
Alda Marliana |
Alfa Ginting  |
Alifianda Rifqy |
Alverina Susan  |
Alvin Lukman Nulhakim |
Alvira Nurul A  |
Ananda Alifka |
Andhika Putra Bagaskara |
Andi M Ivan |
Andi Maryadi  |
Andreas Ikun  |
Andy Satria |
Angga Fajar Pratama |
Annisya N F |
Ari Fahmi Wisuda Pratama  |
Aria Pratomo  | ...
            </p>
        </div>
      </div>
      <div class="button"><a href="<?=base_url();?>halaman/angkatan/11">SELENGKAPNYA...</a></div>
    </div><!-- end .content -->

  </div><!-- end .container -->
</div><!-- end .tentang-sadaya -->

<div class="section tentang-sadaya" id="tentang">
  <div class="container">
    <div class="section-title">TENTANG <Strong>SADAYA.</Strong></div>
    <div class="divider"></div>

    <div class="content">
      <div class="row">
        <div class="logo"><img src="assets/images/logo-mini.png"></div>
        <div class="text">
          <p>Saung Budaya (SADAYA) UNIKOM merupakan salah satu Unit Kegiatan Mahasiswa yang ada di Universitas Komputer Indonesia yang bergerak di bidang pelestarian dan perkembangan seni budaya khususnya kesenian tradisional Indonesia.  Sejak berdiri pada tanggal 25 Februari 2008, SADAYA telah mengembangkan berbagai kesenian tradisional & kontemporer diantaranya seperti paduan alat musik angklung, tarian-tarian daerah, perkusi dan rampak kendang. SADAYA terletak di kampus 2 Lantai 5 UNIKOM.</p>
        </div>
      </div>
      <div class="button"><a href="tentang.php">SELENGKAPNYA...</a></div>

    </div><!-- end .content -->

  </div><!-- end .container -->
</div><!-- end .tentang-sadaya -->

<div class="section divisi-sadaya" id="divisi">
  <div class="container">
    <div class="section-title">DIVISI <Strong>SADAYA.</Strong></div>
    <div class="divider"></div>

    <div class="content">
      <div class="row">
        <div class="box">
          <div class="cont-img">
            <div class="image"><img src="assets/images/angklung.png"></div>
          </div>
          <div class="title">Angklung</div>
        </div>
        <div class="box">
          <div class="cont-img">
            <div class="image"><img src="assets/images/gamelan.png"></div>
          </div>
          <div class="title">GAMELAN</div>
        </div>
        <div class="box">
          <div class="cont-img">
            <div class="image"><img src="assets/images/perkusi.png"></div>
          </div>
          <div class="title">PERKUSI</div>
        </div>
        <div class="box">
          <div class="cont-img">
            <div class="image"><img src="assets/images/kendang.png"></div>
          </div>
          <div class="title">RAMPAK KENDANG</div>
        </div>
        <div class="box">
          <div class="cont-img">
            <div class="image"><img src="assets/images/tari.png"></div>
          </div>
          <div class="title">TARI</div>
        </div>
      </div>
    </div><!-- end .content -->

  </div><!-- end .container -->
</div><!-- end .divisi-sadaya -->

<div class="section prestasi" id="prestasi">
  <div class="container">
    <div class="section-title">PRESTASI <Strong>SADAYA.</Strong></div>
    <div class="divider"></div>

    <div class="content">
      <div class="row">
        <ul>
        <li><span>Wakil Indonesia dalam Pentas Seni Lestari yang diselenggarakan oleh Fuchun Community Club di Singapura pada tahun 2011.</span></li>
        <li><span>Penampil Terbaik II dalam Festival Pagelaran Seni Budaya Tingkat Perguruan Tinggi se-ASEAN Plus China, Jepang, dan Korea Selatan yang diselenggarakan oleh Kementrian Kebudayaan dan Pariwisata Republik Indonesia pada tahun 2010.</span></li>
        <li><span>Lima Penampil Terbaik dalam Festival Kesenian Daerah antar perguruan tinggi Se Jawa Barat dan Banten dalam rangka rangkaian peringatan Hari Pendidikan Nasional 2009.</span></li>
        <li><span>Pemenang II Festival Paduan Angklung ITB 2011 untuk kategori Umum Se Jawa Barat, Banten dan DKI Jakarta.</span></li>
        <li><span>Pemenang II Lomba Perkusi Yamaha se Jawa Barat tahun 2011.</span></li>
        <li><span>Setiap tahunnya menjadi pengisi acara dalam berbagai kegiatan internal kampus, seperti Penerimaan Mahasiswa Baru, Wisuda Unikom, Open House Unikom, Penerimaan Mahasiswa Asing Program Darmasiswa Unikom dan lain-lain.</span></li>
        <li><span>Pengisi Acara dalam acara Jambore Nasional IV Paguyuban Honda Vario Nusantara pada tahun 2013 di Sasana Budaya Ganesha Bandung.</span></li>
        <li><span>Pengisi acara dalam event “Riung Mungpulung Tur Tepang Wanoh II” Vespa Antique Club Indonesia pada tahun 2010 di Bandung Kulon.</span></li>
        <li><span>Setiap tahunya mengadakan Pentas Seni dalam rangka memperingati HUT Sadaya. Pentas Seni dengan tema Harmoni Budaya Bertempat di Dago Tea House.</span></li>
        <li><span>Pemenang III Lomba Angklung Festival Budaya Sunda Trans Studio Bandung tahun 2014.</span></li>
        <li><span>Pemenang Harapan II dalam Festival Paduan Angklung ITB XIV untuk Kategori Umum pada tahun 2015.</span></li>
        <li><span>Peserta Angklung Day 2016 @Gedung Sate</span></li>
        <li><span>Pengisi Acara di Festival Indonesia Menggugat 2016</span></li>
        <li><span>Pemecah Original Rekor Indonesia kategori musik internal dan oratrium kolosal di Festival Babakan Siliwangi 2016</span></li>
        <li><span>Juara 3 Angklung tingkat nasional di Festival Paduan Angklung XV ITB 2017</span></li>
        
        <div class="button"><a href="tentang.php#prestasi">SELENGKAPNYA...</a></div>

        </ul>
      </div>
    </div><!-- end .content -->

  </div><!-- end .container -->
</div><!-- end .divisi-sadaya -->
  

<div class="section galeri" id="galeri">
  <div class="container">
    <div class="section-title">GALERI <Strong>SADAYA.</Strong></div>
    <div class="divider"></div>

    <div class="content">
      <div class="row">
        <div class="box"><img src="assets/images/p1.jpg"></div>
        <div class="box"><img src="assets/images/p2.jpg"></div>
        <div class="box"><img src="assets/images/p3.jpg"></div>
        <div class="box"><img src="assets/images/p4.jpg"></div>
      </div>
      <div class="row">
        <div class="box"><img src="assets/images/p5.jpg"></div>
        <div class="box"><img src="assets/images/p6.jpg"></div>
        <div class="box"><img src="assets/images/p7.jpg"></div>
        <div class="box"><img src="assets/images/rk.jpg"></div>
      </div>

      <div class="button"><a href="">LEBIH BANYAK GALERI</a></div>
    </div><!-- end .content -->

  </div><!-- end .container -->
</div><!-- end .galeri -->

<style type="text/css">
   .moto-sadaya{
    background: url('assets/images/pallet.jpg') fixed no-repeat;
   }
</style>

<div class="section moto-sadaya" data-500-bottom="background-position: 0% -100%"
 data-top-bottom="background-position: 0% 100%">
  <div class="container">
    
      <div class="content">
        <span class="text">Mari Bergabung Bersama Kami</span>
        <span class="child"><i>Bersama Sadaya, Kita Bangkitkan Kesenian Tradisional</i></span>

      <div class="button"><a href="daftar.php">BERGABUNG SEKARANG</a></div>
      </div><!-- end .content -->

    </div><!-- end .container -->
</div>


<div class="section kontak" id="kontak">
  <div class="container">
    <div class="section-title">KONTAK <Strong>SADAYA.</Strong></div>
    <div class="divider"></div>

    <div class="content">
      <div class="row">
        <div class="box right google-maps">
          <iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.0292821582225!2d107.61287131431753!3d-6.887095995023358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e656039e072f%3A0x254eac491837e2c1!2sUnikom%2CJl.Dipatiukur!5e0!3m2!1sid!2sid!4v1493207705314" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <div class="box">
          <ul>
<li><div class="logo"><i style="top:13px; left:14px " class="fa fa-phone"></i></div>  <span>0896-7623-2129 / 0857-7225-5010</span></li>
<li><div class="logo"><i style="top:11px; left:11px " class="fa fa-envelope"></i></div> <span>sadayaunikom@gmail.com</span></li>
<li><div class="logo"><i style="top:11px; left:13px " class="fa fa-instagram"></i></div> <span>@SADAYA_UNIKOM</span></li>
<li><div class="logo"><i style="top:12px; left:12px " class="fa fa-twitter"></i></div> <span>@SADAYA_</span></li>
<li><div class="logo"><i style="top:12px; left:13px " class="fa fa-address-book"></i></div> <span> Rorompok SADAYA <br>Kampus 2 Lantai 5 UNIKOM<Br> Jl.Dipatiukur No. 116 Bandung, <br>40132</span></li>
          </ul>
        </div>
      </div>
    </div>  
    
  </div><!-- end .container -->
</div><!-- end .galeri -->
