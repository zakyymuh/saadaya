
  <div class="footer">
  <div class="row">
    <div class="copyright">MADE WITH <i style="color: #bda168" class="fa fa-coffee"></i> & <i style="color: #a24848" class="fa fa-heart"></i> BY <a style="font-weight:bold" href="developer.php">SADAYA DEV TEAM</a></div>
      <ul class="sosmed">
      <li><a href="mailto:sdnkebongedang01@gmail.com"><div class="logo email"><i style="top:7px; left:7px " class="fa fa-envelope"></i></div></a></li>
      <li><a href="instagram.com/SADAYA_UNIKOM"><div class="logo instagram"><i style="top:7px; left:9px " class="fa fa-instagram"></i></div></a></li>
      <li><a href="https://www.facebook.com/groups/154539557925252/?fref=ts"><div class="logo facebook"><i style="top:8px; left:10px " class="fa fa-facebook"></i></div></a></li>
      <li><a href="https://twitter.com/sadaya_"><div class="logo twitter"><i style="top:8px; left:8px " class="fa fa-twitter"></i></div></a></li>
      </ul>
  </div>
</div>
<script type="text/javascript" src="<?=base_url();?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/mainjs.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/skrollr/dist/skrollr.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/pnotify/pnotify.custom.min.js"></script>