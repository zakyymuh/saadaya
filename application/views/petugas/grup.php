<style>
	.big-box {
	width: 100%;
	}
</style>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Grup
            <small>Kelola semua grup</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url();?>petugas/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">grup</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
			<a href="<?= base_url();?>petugas/grup_utama/"><div class="col-lg-3 col-xs-6">
              <!-- small box -->
			  <div class="info-box ">
                <span class="info-box-icon big-box bg-yellow"><i class="ion ion-ios-list"></i>&nbsp;Daftar</span>
                <!-- /.info-box-content -->
              </div>
            </div></a><!-- ./col -->
            <a href="<?= base_url();?>petugas/tambah_grup/"><div class="col-lg-3 col-xs-6">
              <!-- small box -->
			  <div class="info-box ">
                <span class="info-box-icon big-box bg-green"><i class="ion ion-ios-people"></i>&nbsp;Tambah</span>
                <!-- /.info-box-content -->
              </div>
            </div></a>
			<a href="<?= base_url();?>petugas/suggest_grup/"><div class="col-lg-3 col-xs-6">
              <!-- small box -->
			  <div class="info-box ">
                <span class="info-box-icon big-box bg-aqua"><i class="ion ion-android-bulb"></i>&nbsp;Suggest</span>
                <!-- /.info-box-content -->
              </div>
            </div></a><!-- ./col -->
			<a href="<?= base_url();?>petugas/laporan_grup/"><div class="col-lg-3 col-xs-6">
              <!-- small box -->
			  <div class="info-box ">
                <span class="info-box-icon big-box bg-red"><i class="ion ion-alert"></i>&nbsp;Laporan</span>
                <!-- /.info-box-content -->
              </div>
            </div></a><!-- ./col -->
            </div>
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-7">
              <!-- Custom tabs (Charts with tabs)-->
             <!-- quick email widget -->
              <div class="box box-info">
                <div class="box-header">
                  <i class="fa fa-envelope"></i>
                  <h3 class="box-title">Quick Email</h3>
                </div>
                <div class="box-body">
                  <form action="#" method="post">
                    <div class="form-group">
                      <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" name="subject" placeholder="Subject">
                    </div>
                    <div>
                      <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                  </form>
                </div>
                <div class="box-footer clearfix">
                  <button class="pull-right btn btn-default" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button>
                </div>
              </div>
			  </section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5">

              

              <!-- Chat box -->
              <div class="box box-success">
                <div class="box-header">
                  <i class="fa fa-bullhorn"></i>
                  <h3 class="box-title">Informasi</h3>
                </div>
                <div class="box-body chat" id="chat-box">
                  <!-- chat item -->
                  <div class="item">
                    <img src="<?= base_url();?>/assets/image/upload/default.jpg" alt="user image" class="offline">
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 01:11</small>
                        <a>Petugas Tampan</a><br>
                      </a> Menambahkan grup <a>PENCAK SILAT</a> di grup <a>SMKN 11 BDG</a>.
                    </p>
                  </div><!-- /.item -->
				  <div class="item">
                    <img src="<?= base_url();?>/assets/image/upload/default.jpg" alt="user image" class="offline">
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 01:05</small>
                        <a>Petugas Tampan</a><br>
                      </a> Merubah nama grup <a>SMK NEGERI 11 BDG</a> menjadi <a>SMKN 11 BDG</a>.
                    </p>
                  </div><!-- /.item -->
				  <div class="item">
                    <img src="<?= base_url();?>/assets/image/upload/default.jpg" alt="user image" class="offline">
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> kemarin</small>
                        <a>Petugas Tampan</a><br>
                      </a> Konfirmasi Suggest dari <a>David</a>, membuat subgrup "<a>AA - 08</a>".
                    </p>
                  </div><!-- /.item --><div class="item">
                    <img src="<?= base_url();?>/assets/image/upload/default.jpg" alt="user image" class="offline">
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2 bulan</small>
                        <a>Petugas Baik Hati</a><br>
                      </a> Konfirmasi Suggest dari <a>Bambang</a>, membuat subgrup "<a>AA - 01</a>".
                    </p>
                  </div><!-- /.item -->
                  <!-- chat item -->
                  <div class="item">
                    <img src="<?= base_url();?>/assets/image/upload/default.jpg" alt="user image" class="offline">
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2 bulan</small>
                        <a>Petugas Kece</a><br>
                      </a>Menghapus grup "<a>KEKECEAN BADAI</a>" dari list grup. Alasan : Menyalahi aturan grup no 13.
					  </p>
                  </div><!-- /.item -->
                </div><!-- /.chat -->
                <div class="box-footer">
                  <div class="input-group">
                    <input class="form-control" placeholder="Tulis Pengumuman baru ..">
                    <div class="input-group-btn">
                      <button class="btn btn-success"><i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                </div>
              </div><!-- /.box (chat box) -->
            </section><!-- right col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

