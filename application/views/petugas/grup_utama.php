<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Daftar grup utama
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?= base_url();?>petugas/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=base_url();?>petugas/grup/">grup</a></li>
            <li class="active">Grup utama</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
		<a href="<?= base_url();?>petugas/grup_utama/"><button class="btn bg-red btn-flat margin"><i class="ion ion-ios-list"></i>&nbsp; Grup Utama</button></a>
		<a href="<?= base_url();?>petugas/grup_tambahan/"><button class="btn bg-maroon btn-flat margin"><i class="ion ion-ios-list"></i>&nbsp; Grup Tambahan</button></a>
		<div class="box box-info">
          <div class="box-body">
                  <table id="example1" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Gambar</th>
                        <th>Nama</th>
                        <th>Tahun</th>
                        <th>Alamat</th>
                        <th>Keterangan</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php
						foreach($grup as $row){
						?>
						<tr>
							<td class="text-center"><img src="<?= base_url();?>assets/image/upload/<?= $row->gambar;?>" class="img-circle" alt="User Image"></td>
							<td><a href="<?= base_url();?>petugas/detail/<?=$row->id_grup;?>" class="text-default"><span data-toggle="tooltip" title="Click for detail" data-original-title="Detail Grup"><?= $row->nama;?></span></a></td>
							<td><?= $row->tahun;?></td>
							<td><?= $row->alamat;?></td>
							<td><?= $row->keterangan;?></td>
						  </tr>
						  <?php
						}
					?>
                    </tbody> 
					<tfoot>
                      <tr>
                        <th>Gambar</th>
                        <th>Nama</th>
                        <th>Tahun</th>
                        <th>Alamat</th>
                        <th>Keterangan</th>
                      </tr>
                    </tfoot>
					</table>
		  </div>
		  </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
		<script src="<?= base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script>
		$(function(){
		$("#example1").DataTable();
        });
		</script>