
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Your recent activity</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
			  <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-android-contact"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">User registered</span>
                  <span class="info-box-number">153.220</span>
                </div><!-- /.info-box-content -->
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6"> <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-android-contacts"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Grup registered</span>
                  <span class="info-box-number">31.023</span>
                </div><!-- /.info-box-content -->
              </div>
              <!-- small box -->
            </div><!-- ./col -->
              <!-- small box --> <div class="col-lg-3 col-xs-6"> <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-calendar-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">events registered</span>
                  <span class="info-box-number">153</span>
                </div><!-- /.info-box-content -->
              </div>
              <!-- small box -->
            </div>
              <!-- small box --> <div class="col-lg-3 col-xs-6"> <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-ios-paper-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Article post</span>
                  <span class="info-box-number">209</span>
                </div><!-- /.info-box-content -->
              </div>
              <!-- small box --></div>
            </div>
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-7">
              <!-- Custom tabs (Charts with tabs)-->
             <!-- quick email widget -->
              <div class="box box-info">
                <div class="box-header">
                  <i class="fa fa-envelope"></i>
                  <h3 class="box-title">Quick Email</h3>
                </div>
                <div class="box-body">
                  <form action="#" method="post">
                    <div class="form-group">
                      <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" name="subject" placeholder="Subject">
                    </div>
                    <div>
                      <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                  </form>
                </div>
                <div class="box-footer clearfix">
                  <button class="pull-right btn btn-default" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button>
                </div>
              </div>
			  </section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5">

              

              <!-- Chat box -->
              <div class="box box-success">
                <div class="box-header">
                  <i class="fa fa-bullhorn"></i>
                  <h3 class="box-title">Informasi</h3>
                </div>
                <div class="box-body chat" id="chat-box">
                  <!-- chat item -->
                  <div class="item">
                    <img src="dist/img/user3-128x128.jpg" alt="user image" class="offline">
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:15</small>
                        Alexander Pierce
                      </a>
                      I would like to meet you to discuss the latest news about
                      the arrival of the new theme. They say it is going to be one the
                      best themes on the market
                    </p>
                  </div><!-- /.item -->
                  <!-- chat item -->
                  <div class="item">
                    <img src="dist/img/user2-160x160.jpg" alt="user image" class="offline">
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
                        Susan Doe
                      </a>
                      I would like to meet you to discuss the latest news about
                      the arrival of the new theme. They say it is going to be one the
                      best themes on the market
                    </p>
                  </div><!-- /.item -->
                </div><!-- /.chat -->
                <div class="box-footer">
                  <div class="input-group">
                    <input class="form-control" placeholder="Tulis Pengumuman baru ..">
                    <div class="input-group-btn">
                      <button class="btn btn-success"><i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                </div>
              </div><!-- /.box (chat box) -->
            </section><!-- right col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

