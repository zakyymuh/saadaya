<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <script src="<?= base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url();?>assets/source/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/dist/css/AdminLTE.min.css">
  
<style>
 .no-border-radius {
  border-radius : 0;
 }
 .box-footer {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
    border-top: 1px solid #f4f4f4;
    padding: 10px 0 0 0;
    background-color: #fff
 }
</style>
</head>
<body>
<?php
if($this->session->flashdata('item')) {
$message = $this->session->flashdata('item');
?>
<div class="container-fluid text-center no-border-radius alert <?= $message['class'];?> fade in affix"><?php echo $message['message']; ?>
</div>
<?php } ?>
<div class="login-box">
 <div class="login-header text-center">
      <a href='<?php echo base_url();?>'><strong>HELLO </strong>PETUGAS</a>
 </div>
 <div class="login-body">
 <?= form_open("login/petugas") ?>
  <div class="input-group">
  <span class="input-group-addon">
				<span class="fa fa-user"></span>
				</span>
				<input type="text" name="username" class="form-control" id="username" name="Nama Lengkap" placeholder="Username" alt="username" required>
  </div>
    <div class="input-group">
   <span class="input-group-addon">
				<span class="fa fa-lock"></span>
				</span>
				<input type="password" name="password" class="form-control" id="password" name="password" Placeholder='Password' required>
  </div>
<div class="box-footer">
	<div>
                    <button type="submit" class="btn btn-primary btn-flat" name='submit' value='masuk'>Submit
					</button>
					
                  </div>
                  </div>
  
 </form>
 <div class="tambahan-link">
 <label class='small'><a href="#" data-toggle="tooltip" title="Lupa password belum tersedia :)">Lupa Password</a></label>
 </div>
 </div>
</div>
</body>
</html>
<script src='<?php echo base_url();?>assets/source/jquery-ui.min.js'></script>
<script src='<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js'></script>