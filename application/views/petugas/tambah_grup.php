<style>
	.pointer {
		cursor:pointer;
	}
	.pointer:hover td {
		color:green;
	}
</style>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
		  <h1>
			Tambah baru.
		  </h1>
          <ol class="breadcrumb">
            <li><a href="<?= base_url();?>petugas/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Grup</li>
            <li class="active">Tambah grup</li>
          </ol>
        </section>
   
        <!-- Main content -->
        <section class="content">
		<div class="row">
            <!-- left column -->
            <div class="col-md-6">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Grup Utama</a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Grup Tambahan</a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                <!-- form start -->
				<?php $att = array("name"=>"form_grup_utama");?>
                <?= form_open("petugas/tambah_grup_utama",$att);?>
				<div class="box-body">
                    <div class="form-group">
                      <label for="Namagrup">Nama grup</label>
                      <input type="text" name="nama" class="form-control" id="Namagrup" placeholder="Masukan nama .." required>
                    </div>
					<label>Provinisi</label>
					<div class="form-group provinsi">
                      <select class="form-control">
							<option>option 1</option>
							<option>option 2</option>
							<option>option 3</option>
							<option>option 4</option>
							<option>option 5</option>
						  </select>    
						</div>
					<div class="form-group">
                      <label for="alamat">Kecamatan / Kabupaten</label>
                    	<select class="form-control">
							<option>option 1</option>
							<option>option 2</option>
							<option>option 3</option>
							<option>option 4</option>
							<option>option 5</option>
						  </select>    
                    </div>
					<div class="form-group">
                      <label for="alamat">Kelurahan</label>
                      <select class="form-control">
							<option>option 1</option>
							<option>option 2</option>
							<option>option 3</option>
							<option>option 4</option>
							<option>option 5</option>
						  </select>  
                    </div>
					<div class="form-group">
                      <label for="alamat">Kecamatan</label>
                      <select class="form-control">
							<option>option 1</option>
							<option>option 2</option>
							<option>option 3</option>
							<option>option 4</option>
							<option>option 5</option>
						  </select>  
                    </div>
					<div class="form-group">
                      <label for="alamat">Alamat lengkap</label>
                      <input type="text" name="alamat" class="form-control" id="Alamat" placeholder="Masukan alamat .." required>
                    </div>
					<div class="form-group">
                      <label for="tahun">Tahun</label>
                      <input type="number" name="tahun" class="form-control" id="tahun" placeholder="2016" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Gambar grup</label>
                      <input type="file" id="exampleInputFile">
                    </div>
                    <div class="form-group">
                      <label for="InputKeterangan">Keterangan tambahan</label>
					  <textarea class="form-control" name='keterangan' rows="3" placeholder="Masukan keterangan ..."></textarea>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" name='submit' value="submit" class="btn btn-primary btn-flat pull-right">Submit</button>
                  </div>
                </form>
                  </div><!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
				  <?php $att = array("onsubmit"=>"return cek_utama()")?>
                  <?= form_open("petugas/tambah_grup_tambahan/",$att) ?>
                  <div class="box-body"> <label for="Namagrup">Pilih grup utama</label>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Alamat</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php
						foreach($utama as $row){
						?>
						<tr class="pointer" onclick='pilih("<?= $row->id_grup;?>","<?= $row->nama;?>")'>
							<td ><i class='fa fa-check hoverGreen'></i>&nbsp;<?= $row->nama;?></td><td><?= $row->alamat;?></td>
						  </tr>
						  <?php
						}
					?>
                    </tbody>
                  </table>
					<div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-check text-info"></i></span>
                    <input type="text" class="form-control" id="Namagruputama" placeholder="Pilih dari grup yang telah tersedia .." required>
					<input type="hidden" value="" name="grup_utama" id="grup_pilih">
                  </div><br>
					<div class="form-group">
                      <label for="Namagrup">Nama grup</label>
                      <input type="text" class="form-control" id="Namagrup" name="nama" placeholder="Masukan nama .." required>
                    </div><div class="form-group">
                      <label for="InputKeterangan">Keterangan tambahan</label>
					  <textarea class="form-control" name='keterangan' rows="3" placeholder="Masukan keterangan ..."></textarea>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Gambar grup</label>
                      <input name="image" type="file" id="exampleInputFile">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" name='submit' value= "Simpan" class="btn btn-primary btn-flat pull-right">Submit</button>
                  </div>
                </form>
              </div>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
			  </div>
		  <div class="col-md-6">
				<?php
					if($this->session->flashdata('item')) {
					$message = $this->session->flashdata('item');
					?>
					   <div class='alert <?= $message['class'];?>'><?= $message['message'];?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
					<?php } ?>
			
              <!-- general form elements -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Keterangan</h3>
                </div><!-- /.box-header -->
		  <div class="box-body">
		  <p>1. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
		  <p>2. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
		  <p>3. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
		  </div>
		  </div>
		  </div>
		  </div>
		<script src="<?= base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script>
			function pilih(nilai,nama){
				var nilaikirim = document.getElementById("Namagruputama");
					nilaikirim.value = nama;
				var datahidden = document.getElementById("grup_pilih");
					datahidden.value = nilai;
			}
		$(function(){
		$("#example1").DataTable();
        });
		
		</script>
  
  
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->