<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ALUMNI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/source/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/source/ionicons.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/plugins/morris/morris.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
 <link rel="stylesheet" href="<?php echo base_url();?>assets/css/dataTables.bootstrap.css">
  </head>
  <body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>ALL</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>ALL</b>UMNI</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                     <li data-toggle="tooltip" title="Log out" data-original-title="Log out" data-placement="left">
						<a href="<?= base_url()?>petugas/logout" >
						<i class="text-red fa fa-power-off"></i>
						</a>
					</li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url();?>assets/image/upload/default.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php 
				foreach ($data as $row)
				echo $row->username;
				?>
			  </p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header text-center">NAVIGATION</li>
            <li class="active treeview">
              <a href="<?= base_url();?>petugas/">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
			<li>
              <a href="<?= base_url()?>petugas/">
                <i class="fa fa-user"></i>
                <span>Account</span>
              </a>
            </li>
			<li class="treeview">
              <a href="<?= base_url();?>petugas/grup/">
                <i class="fa fa-users"></i>
                <span>Grup</span>
              </a>
			  </li>			  
            <li class="treeview">
              <a href="#">
                <i class="fa fa-file-text-o"></i>
                <span>Articles</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Tambah Artikel</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Request Artikel</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Laporan Artikel</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Artikel Tertolak</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Daftar Artikel</a></li>
                </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-calendar"></i>
                <span>Event</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Buat Event</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Daftar Event</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Request Event</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Laporan Event</a></li>
                </ul>
            </li>
			 <li class="treeview">
              <a href="#">
                <i class="fa fa-map-marker"></i>
                <span>Lokasi</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Tambah Lokasi</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Daftar Lokasi</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Laporan Lokasi</a></li>
                </ul>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>	  
 <?php $this->load->view("petugas/".$content);?>
      <footer class="main-footer">
        <strong>Copyright &copy; 2016 <a href="#"><strong>ALL</strong>UMNI</a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->
    <script src="<?= base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?= base_url();?>assets/source/jquery-ui.min.js"></script>
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="<?= base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url();?>assets/source/raphael-min.js"></script>
    <script src="<?= base_url();?>assets/plugins/morris/morris.min.js"></script>
    <script src="<?= base_url();?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?= base_url();?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?= base_url();?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?= base_url();?>assets/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="<?= base_url();?>assets/source/moment.min.js"></script>
    <script src="<?= base_url();?>assets/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?= base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?= base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?= base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url();?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url();?>assets/dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?= base_url();?>assets/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?= base_url();?>assets/dist/js/demo.js"></script>
    <script src="<?= base_url();?>assets/dist/js/app.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/prettify.js"></script> 
	<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script> 
	<script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js"></script>
  </body>
</html>
