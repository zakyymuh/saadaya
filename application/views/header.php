<!DOCTYPE html>
<html>
<head>
  <meta name="google-site-verification" content="M2_bYecNNLBYJlvxcf2j8mIKkShnFgXGJLH6UP8F3cY" />
  <title>SADAYA UNIKOM</title>
  <link rel="shortcut icon" href="<?=base_url();?>assets/images/logo.ico" type="image/x-icon">
  <?php if(isset($onPendaftaran)){ ?>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <?php }else{ ?>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/bootsraps-custom/bootstrap.min.css">
  <?php } ?>

  <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/source/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/pnotify/pnotify.custom.min.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/mainstyle.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/normalize.css">

 <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>


<div class="menu" id="<?php echo (isset($onHome))  ? 'menu-top' : 'menu-absolute' ?>">
  <a href="<?=base_url();?>" class="logo-menu"><img src="<?=base_url();?>assets/images/logo-mini.png"></a>
  <div class="togel-menu"><a href=""><i class="fa fa-bars"></i></a></div>

  <ul class='menu-slide' <?php echo (isset($onHome))  ? 'id="menu-fixed"' : '' ?> >
    <li><a href="<?php echo (!isset($onHome)) ? 'index.php' : '' ?>#top">Beranda</a></li>
    <li><a href="<?php echo (!isset($onHome)) ? 'index.php' : '' ?>#tentang">Tentang</a></li>
    <li><a href="<?php echo (!isset($onHome)) ? 'index.php' : '' ?>#divisi">Divisi</a></li>
    <li><a href="<?php echo (!isset($onHome)) ? 'index.php' : '' ?>#prestasi">Prestasi</a></li>
    <li><a href="<?php echo (!isset($onHome)) ? 'index.php' : '' ?>#galeri">Galeri</a></li>
    <li><a href="<?php echo (!isset($onHome)) ? 'index.php' : '' ?>#kontak">Kontak</a></li>
  </ul>
</div>
