$().ready(function() {
	//fungsi togel tombol
	$('.menu .togel-menu').click( function(e) {
		e.preventDefault();	
		if(!$('.menu-slide').hasClass('show')){
			$('.menu-slide').addClass('show');
			$('.togel-menu a').addClass('clicked');
		}else{
			$('.menu-slide').removeClass('show');
			$('.togel-menu a').removeClass('clicked');
		}
	});


	// end function

	//inisialisasi skroolr
	if($(window).width() > 768) {

		var  myHeight = 0;
			if( typeof( window.innerHeight ) == 'number' ) {
				myHeight = window.innerHeight;
			} else if( document.documentElement && document.documentElement.clientHeight ) {
				myHeight = document.documentElement.clientHeight;
			} else if( document.body && (document.body.clientHeight ) ) {
				myHeight = document.body.clientHeight;
			}
		$('.section').css("min-height" , myHeight+"px");

		var s = skrollr.init(); 
	}

	//fungsi hide menu on scrool bottom
	if ((typeof onHome !== 'undefined') && ($(window).width() > 768)) {
		var lastScrollTop = 0;
	 	$(window).scroll(function(event){
	      	var st = $(this).scrollTop();
	      	if (st > lastScrollTop){
	         	$('#menu-top').removeClass("tampil");
	          	$('#menu-top').addClass("sembunyi");
	      	} else {
	          	$('#menu-top').addClass("tampil");
	          	$('#menu-top').removeClass("sembunyi");
	      	}
	      	lastScrollTop = st;

			if ($(window).scrollTop() > 20){
			   $('#menu-top').addClass( "color");
			}
			else {
			   $('#menu-top').removeClass("color");
			   $('#menu-top').removeClass("tampil");
			}

			if ($(window).scrollTop() > 40){
			    $('#menu-top').removeClass("on-top");
			}else{
				$('#menu-top').addClass("on-top");
				}
	   	});

	   	//on reload page
	   	if ($(window).scrollTop() > 60){
	   	   $('#menu-top').addClass( "color");
	   	   $('#menu-top').addClass( "tampil");
	   	}
	   	else {
	   	   $('#menu-top').removeClass("color");
	   	   $('#menu-top').removeClass("tampil");
		}	
	}//end if

	if (typeof onHome !== 'undefined') {
		$('#menu-fixed a').each( function() {
		    var $this = $(this), 
		        target = this.hash;
		    jQuery(this).click(function (e) { 
		        if( $this.length > 0 ) {
		            if($this.attr('href') !== '#' ) {
		            	e.preventDefault();	
		               $('html, body').animate({ 
		                    scrollTop: ($(target).offset().top) 
		                }, 1000);
		            }  
		        }
		    });
		}); 
	}


	if(($(window).width() > 768) && (typeof onHome != 'undefined')) {
		function isElementInViewport(elem) {
		    var element = $(elem);

		    var top_of_element = element.offset().top;
	    	var bottom_of_element = element.offset().top + element.outerHeight();
		    var bottom_of_screen = $(window).scrollTop() + $(window).height();
		    var top_of_screen = $(window).scrollTop();
		    var valid = (bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element);
		    //console.log(valid);
		    return valid;
		}

		function checkAnimation(param) {
		    var element = param;
		    //console.log(param);

		    if (element.hasClass('start')) return; 

		    if (isElementInViewport(element)) {
		        var index = 0;
		        var delay = setInterval( function(){
		          if (index <= element.length){
		            $( element[ index ] ).addClass( 'start');//.removeClass('transparant');
		            index += 1;
		          }else{
		            clearInterval( delay );
		          }
		        }, 200 ); // 0,2 detik
		    
		    }
		}

		//intial opacity

		var is_safari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

		//cek animation

		function SupportedAnimation(){
			var animation = false,
			    animationstring = 'animation',
			    keyframeprefix = '',
			    domPrefixes = 'Webkit Moz O ms Khtml'.split(' '),
			    pfx  = '',
			    elm = document.createElement('div');

			if( elm.style.animationName !== undefined ) { animation = true; }    

			if( animation === false ) {
			  for( var i = 0; i < domPrefixes.length; i++ ) {
			    if( elm.style[ domPrefixes[i] + 'AnimationName' ] !== undefined ) {
			      pfx = domPrefixes[ i ];
			      animationstring = pfx + 'Animation';
			      keyframeprefix = '-' + pfx.toLowerCase() + '-';
			      animation = true;
			      break;
			    }
			  }
			}
			return animation;
		}

		if(SupportedAnimation()){
		    $('.tentang-sadaya .logo').addClass('transparant');
		    $('.tentang-sadaya .text').addClass('transparant');
		    $('.divisi-sadaya .box').addClass('transparant');
		    $('.prestasi li').addClass('transparant');
		    $('.galeri .box').addClass('transparant');
		    $('.kontak li').addClass('transparant');
		    $('.kontak .right').addClass('transparant');
		}

		$(window).scroll(function(){
		    checkAnimation( $('.tentang-sadaya .logo') );
		    checkAnimation( $('.tentang-sadaya .text') );
		    checkAnimation( $('.divisi-sadaya .box') );
		    checkAnimation( $('.prestasi li') );
		    checkAnimation( $('.galeri .box') );
		    checkAnimation( $('.kontak li') );
		    checkAnimation( $('.kontak .right') );
		});


	} 
});//end document ready function